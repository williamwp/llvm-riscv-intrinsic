RISCVBuiltin(vsetvl, "zzz", "", 0, 1, 2, )

RISCVBuiltin(vsetvlmax, "zz", "", 0, 1, )

RISCVBuiltin(vle8_v_i8m1, "q1ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m1, "vq1ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m2, "q2ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m2, "vq2ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m4, "q4ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m4, "vq4ScSc*", "", 2, )

RISCVBuiltin(vle8_v_i8m8, "q8ScScC*", "", 1, )

RISCVBuiltin(vse8_v_i8m8, "vq8ScSc*", "", 2, )

RISCVBuiltin(vle16_v_i16m1, "q1SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m1, "vq1SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m2, "q2SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m2, "vq2SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m4, "q4SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m4, "vq4SsSs*", "", 2, )

RISCVBuiltin(vle16_v_i16m8, "q8SsSsC*", "", 1, )

RISCVBuiltin(vse16_v_i16m8, "vq8SsSs*", "", 2, )

RISCVBuiltin(vle32_v_i32m1, "q1ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m1, "vq1ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m2, "q2ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m2, "vq2ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m4, "q4ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m4, "vq4ZiZi*", "", 2, )

RISCVBuiltin(vle32_v_i32m8, "q8ZiZiC*", "", 1, )

RISCVBuiltin(vse32_v_i32m8, "vq8ZiZi*", "", 2, )

RISCVBuiltin(vle64_v_i64m1, "q1WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m1, "vq1WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m2, "q2WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m2, "vq2WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m4, "q4WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m4, "vq4WiWi*", "", 2, )

RISCVBuiltin(vle64_v_i64m8, "q8WiWiC*", "", 1, )

RISCVBuiltin(vse64_v_i64m8, "vq8WiWi*", "", 2, )

RISCVBuiltin(vle8_v_u8m1, "q1UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m1, "vq1UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m2, "q2UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m2, "vq2UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m4, "q4UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m4, "vq4UcUc*", "", 2, )

RISCVBuiltin(vle8_v_u8m8, "q8UcUcC*", "", 1, )

RISCVBuiltin(vse8_v_u8m8, "vq8UcUc*", "", 2, )

RISCVBuiltin(vle16_v_u16m1, "q1UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m1, "vq1UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m2, "q2UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m2, "vq2UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m4, "q4UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m4, "vq4UsUs*", "", 2, )

RISCVBuiltin(vle16_v_u16m8, "q8UsUsC*", "", 1, )

RISCVBuiltin(vse16_v_u16m8, "vq8UsUs*", "", 2, )

RISCVBuiltin(vle32_v_u32m1, "q1UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m1, "vq1UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m2, "q2UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m2, "vq2UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m4, "q4UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m4, "vq4UZiUZi*", "", 2, )

RISCVBuiltin(vle32_v_u32m8, "q8UZiUZiC*", "", 1, )

RISCVBuiltin(vse32_v_u32m8, "vq8UZiUZi*", "", 2, )

RISCVBuiltin(vle64_v_u64m1, "q1UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m1, "vq1UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m2, "q2UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m2, "vq2UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m4, "q4UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m4, "vq4UWiUWi*", "", 2, )

RISCVBuiltin(vle64_v_u64m8, "q8UWiUWiC*", "", 1, )

RISCVBuiltin(vse64_v_u64m8, "vq8UWiUWi*", "", 2, )

RISCVBuiltin(vle32_v_f32m1, "q1ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m1, "vq1ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m2, "q2ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m2, "vq2ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m4, "q4ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m4, "vq4ff*", "", 2, )

RISCVBuiltin(vle32_v_f32m8, "q8ffC*", "", 1, )

RISCVBuiltin(vse32_v_f32m8, "vq8ff*", "", 2, )

RISCVBuiltin(vle64_v_f64m1, "q1ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m1, "vq1dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m2, "q2ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m2, "vq2dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m4, "q4ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m4, "vq4dd*", "", 2, )

RISCVBuiltin(vle64_v_f64m8, "q8ddC*", "", 1, )

RISCVBuiltin(vse64_v_f64m8, "vq8dd*", "", 2, )

RISCVBuiltin(vmv_x_s_i8m1_i8, "Scq1Sc", "", )

RISCVBuiltin(vmv_s_x_i8m1, "q1Scq1ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m2_i8, "Scq2Sc", "", )

RISCVBuiltin(vmv_s_x_i8m2, "q2Scq2ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m4_i8, "Scq4Sc", "", )

RISCVBuiltin(vmv_s_x_i8m4, "q4Scq4ScSc", "", )

RISCVBuiltin(vmv_x_s_i8m8_i8, "Scq8Sc", "", )

RISCVBuiltin(vmv_s_x_i8m8, "q8Scq8ScSc", "", )

RISCVBuiltin(vmv_x_s_i16m1_i16, "Ssq1Ss", "", )

RISCVBuiltin(vmv_s_x_i16m1, "q1Ssq1SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m2_i16, "Ssq2Ss", "", )

RISCVBuiltin(vmv_s_x_i16m2, "q2Ssq2SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m4_i16, "Ssq4Ss", "", )

RISCVBuiltin(vmv_s_x_i16m4, "q4Ssq4SsSs", "", )

RISCVBuiltin(vmv_x_s_i16m8_i16, "Ssq8Ss", "", )

RISCVBuiltin(vmv_s_x_i16m8, "q8Ssq8SsSs", "", )

RISCVBuiltin(vmv_x_s_i32m1_i32, "Ziq1Zi", "", )

RISCVBuiltin(vmv_s_x_i32m1, "q1Ziq1ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m2_i32, "Ziq2Zi", "", )

RISCVBuiltin(vmv_s_x_i32m2, "q2Ziq2ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m4_i32, "Ziq4Zi", "", )

RISCVBuiltin(vmv_s_x_i32m4, "q4Ziq4ZiZi", "", )

RISCVBuiltin(vmv_x_s_i32m8_i32, "Ziq8Zi", "", )

RISCVBuiltin(vmv_s_x_i32m8, "q8Ziq8ZiZi", "", )

RISCVBuiltin(vmv_x_s_i64m1_i64, "Wiq1Wi", "", )

RISCVBuiltin(vmv_s_x_i64m1, "q1Wiq1WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m2_i64, "Wiq2Wi", "", )

RISCVBuiltin(vmv_s_x_i64m2, "q2Wiq2WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m4_i64, "Wiq4Wi", "", )

RISCVBuiltin(vmv_s_x_i64m4, "q4Wiq4WiWi", "", )

RISCVBuiltin(vmv_x_s_i64m8_i64, "Wiq8Wi", "", )

RISCVBuiltin(vmv_s_x_i64m8, "q8Wiq8WiWi", "", )

RISCVBuiltin(vmv_x_s_u8m1_u8, "Ucq1Uc", "", )

RISCVBuiltin(vmv_s_x_u8m1, "q1Ucq1UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m2_u8, "Ucq2Uc", "", )

RISCVBuiltin(vmv_s_x_u8m2, "q2Ucq2UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m4_u8, "Ucq4Uc", "", )

RISCVBuiltin(vmv_s_x_u8m4, "q4Ucq4UcUc", "", )

RISCVBuiltin(vmv_x_s_u8m8_u8, "Ucq8Uc", "", )

RISCVBuiltin(vmv_s_x_u8m8, "q8Ucq8UcUc", "", )

RISCVBuiltin(vmv_x_s_u16m1_u16, "Usq1Us", "", )

RISCVBuiltin(vmv_s_x_u16m1, "q1Usq1UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m2_u16, "Usq2Us", "", )

RISCVBuiltin(vmv_s_x_u16m2, "q2Usq2UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m4_u16, "Usq4Us", "", )

RISCVBuiltin(vmv_s_x_u16m4, "q4Usq4UsUs", "", )

RISCVBuiltin(vmv_x_s_u16m8_u16, "Usq8Us", "", )

RISCVBuiltin(vmv_s_x_u16m8, "q8Usq8UsUs", "", )

RISCVBuiltin(vmv_x_s_u32m1_u32, "UZiq1UZi", "", )

RISCVBuiltin(vmv_s_x_u32m1, "q1UZiq1UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m2_u32, "UZiq2UZi", "", )

RISCVBuiltin(vmv_s_x_u32m2, "q2UZiq2UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m4_u32, "UZiq4UZi", "", )

RISCVBuiltin(vmv_s_x_u32m4, "q4UZiq4UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u32m8_u32, "UZiq8UZi", "", )

RISCVBuiltin(vmv_s_x_u32m8, "q8UZiq8UZiUZi", "", )

RISCVBuiltin(vmv_x_s_u64m1_u64, "UWiq1UWi", "", )

RISCVBuiltin(vmv_s_x_u64m1, "q1UWiq1UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m2_u64, "UWiq2UWi", "", )

RISCVBuiltin(vmv_s_x_u64m2, "q2UWiq2UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m4_u64, "UWiq4UWi", "", )

RISCVBuiltin(vmv_s_x_u64m4, "q4UWiq4UWiUWi", "", )

RISCVBuiltin(vmv_x_s_u64m8_u64, "UWiq8UWi", "", )

RISCVBuiltin(vmv_s_x_u64m8, "q8UWiq8UWiUWi", "", )

RISCVBuiltin(vfmv_f_s_f32m1_f32, "fq1f", "", )

RISCVBuiltin(vfmv_s_f_f32m1, "q1fq1ff", "", )

RISCVBuiltin(vfmv_f_s_f32m2_f32, "fq2f", "", )

RISCVBuiltin(vfmv_s_f_f32m2, "q2fq2ff", "", )

RISCVBuiltin(vfmv_f_s_f32m4_f32, "fq4f", "", )

RISCVBuiltin(vfmv_s_f_f32m4, "q4fq4ff", "", )

RISCVBuiltin(vfmv_f_s_f32m8_f32, "fq8f", "", )

RISCVBuiltin(vfmv_s_f_f32m8, "q8fq8ff", "", )

RISCVBuiltin(vfmv_f_s_f64m1_f64, "dq1d", "", )

RISCVBuiltin(vfmv_s_f_f64m1, "q1dq1dd", "", )

RISCVBuiltin(vfmv_f_s_f64m2_f64, "dq2d", "", )

RISCVBuiltin(vfmv_s_f_f64m2, "q2dq2dd", "", )

RISCVBuiltin(vfmv_f_s_f64m4_f64, "dq4d", "", )

RISCVBuiltin(vfmv_s_f_f64m4, "q4dq4dd", "", )

RISCVBuiltin(vfmv_f_s_f64m8_f64, "dq8d", "", )

RISCVBuiltin(vfmv_s_f_f64m8, "q8dq8dd", "", )

RISCVBuiltin(vzero_i8m1, "q1Sc", "", )

RISCVBuiltin(vundefined_i8m1, "q1Sc", "", )

RISCVBuiltin(vzero_i8m2, "q2Sc", "", )

RISCVBuiltin(vundefined_i8m2, "q2Sc", "", )

RISCVBuiltin(vzero_i8m4, "q4Sc", "", )

RISCVBuiltin(vundefined_i8m4, "q4Sc", "", )

RISCVBuiltin(vzero_i8m8, "q8Sc", "", )

RISCVBuiltin(vundefined_i8m8, "q8Sc", "", )

RISCVBuiltin(vzero_i16m1, "q1Ss", "", )

RISCVBuiltin(vundefined_i16m1, "q1Ss", "", )

RISCVBuiltin(vzero_i16m2, "q2Ss", "", )

RISCVBuiltin(vundefined_i16m2, "q2Ss", "", )

RISCVBuiltin(vzero_i16m4, "q4Ss", "", )

RISCVBuiltin(vundefined_i16m4, "q4Ss", "", )

RISCVBuiltin(vzero_i16m8, "q8Ss", "", )

RISCVBuiltin(vundefined_i16m8, "q8Ss", "", )

RISCVBuiltin(vzero_i32m1, "q1Zi", "", )

RISCVBuiltin(vundefined_i32m1, "q1Zi", "", )

RISCVBuiltin(vzero_i32m2, "q2Zi", "", )

RISCVBuiltin(vundefined_i32m2, "q2Zi", "", )

RISCVBuiltin(vzero_i32m4, "q4Zi", "", )

RISCVBuiltin(vundefined_i32m4, "q4Zi", "", )

RISCVBuiltin(vzero_i32m8, "q8Zi", "", )

RISCVBuiltin(vundefined_i32m8, "q8Zi", "", )

RISCVBuiltin(vzero_i64m1, "q1Wi", "", )

RISCVBuiltin(vundefined_i64m1, "q1Wi", "", )

RISCVBuiltin(vzero_i64m2, "q2Wi", "", )

RISCVBuiltin(vundefined_i64m2, "q2Wi", "", )

RISCVBuiltin(vzero_i64m4, "q4Wi", "", )

RISCVBuiltin(vundefined_i64m4, "q4Wi", "", )

RISCVBuiltin(vzero_i64m8, "q8Wi", "", )

RISCVBuiltin(vundefined_i64m8, "q8Wi", "", )

RISCVBuiltin(vzero_u8m1, "q1Uc", "", )

RISCVBuiltin(vundefined_u8m1, "q1Uc", "", )

RISCVBuiltin(vzero_u8m2, "q2Uc", "", )

RISCVBuiltin(vundefined_u8m2, "q2Uc", "", )

RISCVBuiltin(vzero_u8m4, "q4Uc", "", )

RISCVBuiltin(vundefined_u8m4, "q4Uc", "", )

RISCVBuiltin(vzero_u8m8, "q8Uc", "", )

RISCVBuiltin(vundefined_u8m8, "q8Uc", "", )

RISCVBuiltin(vzero_u16m1, "q1Us", "", )

RISCVBuiltin(vundefined_u16m1, "q1Us", "", )

RISCVBuiltin(vzero_u16m2, "q2Us", "", )

RISCVBuiltin(vundefined_u16m2, "q2Us", "", )

RISCVBuiltin(vzero_u16m4, "q4Us", "", )

RISCVBuiltin(vundefined_u16m4, "q4Us", "", )

RISCVBuiltin(vzero_u16m8, "q8Us", "", )

RISCVBuiltin(vundefined_u16m8, "q8Us", "", )

RISCVBuiltin(vzero_u32m1, "q1UZi", "", )

RISCVBuiltin(vundefined_u32m1, "q1UZi", "", )

RISCVBuiltin(vzero_u32m2, "q2UZi", "", )

RISCVBuiltin(vundefined_u32m2, "q2UZi", "", )

RISCVBuiltin(vzero_u32m4, "q4UZi", "", )

RISCVBuiltin(vundefined_u32m4, "q4UZi", "", )

RISCVBuiltin(vzero_u32m8, "q8UZi", "", )

RISCVBuiltin(vundefined_u32m8, "q8UZi", "", )

RISCVBuiltin(vzero_u64m1, "q1UWi", "", )

RISCVBuiltin(vundefined_u64m1, "q1UWi", "", )

RISCVBuiltin(vzero_u64m2, "q2UWi", "", )

RISCVBuiltin(vundefined_u64m2, "q2UWi", "", )

RISCVBuiltin(vzero_u64m4, "q4UWi", "", )

RISCVBuiltin(vundefined_u64m4, "q4UWi", "", )

RISCVBuiltin(vzero_u64m8, "q8UWi", "", )

RISCVBuiltin(vundefined_u64m8, "q8UWi", "", )

RISCVBuiltin(vzero_f32m1, "q1f", "", )

RISCVBuiltin(vundefined_f32m1, "q1f", "", )

RISCVBuiltin(vzero_f32m2, "q2f", "", )

RISCVBuiltin(vundefined_f32m2, "q2f", "", )

RISCVBuiltin(vzero_f32m4, "q4f", "", )

RISCVBuiltin(vundefined_f32m4, "q4f", "", )

RISCVBuiltin(vzero_f32m8, "q8f", "", )

RISCVBuiltin(vundefined_f32m8, "q8f", "", )

RISCVBuiltin(vzero_f64m1, "q1d", "", )

RISCVBuiltin(vundefined_f64m1, "q1d", "", )

RISCVBuiltin(vzero_f64m2, "q2d", "", )

RISCVBuiltin(vundefined_f64m2, "q2d", "", )

RISCVBuiltin(vzero_f64m4, "q4d", "", )

RISCVBuiltin(vundefined_f64m4, "q4d", "", )

RISCVBuiltin(vzero_f64m8, "q8d", "", )

RISCVBuiltin(vundefined_f64m8, "q8d", "", )

RISCVBuiltin(vmv_v_v_i8m1, "q1Scq1Sc", "", )

RISCVBuiltin(vmv_v_x_i8m1, "q1ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m2, "q2Scq2Sc", "", )

RISCVBuiltin(vmv_v_x_i8m2, "q2ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m4, "q4Scq4Sc", "", )

RISCVBuiltin(vmv_v_x_i8m4, "q4ScSc", "", )

RISCVBuiltin(vmv_v_v_i8m8, "q8Scq8Sc", "", )

RISCVBuiltin(vmv_v_x_i8m8, "q8ScSc", "", )

RISCVBuiltin(vmv_v_v_i16m1, "q1Ssq1Ss", "", )

RISCVBuiltin(vmv_v_x_i16m1, "q1SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m2, "q2Ssq2Ss", "", )

RISCVBuiltin(vmv_v_x_i16m2, "q2SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m4, "q4Ssq4Ss", "", )

RISCVBuiltin(vmv_v_x_i16m4, "q4SsSs", "", )

RISCVBuiltin(vmv_v_v_i16m8, "q8Ssq8Ss", "", )

RISCVBuiltin(vmv_v_x_i16m8, "q8SsSs", "", )

RISCVBuiltin(vmv_v_v_i32m1, "q1Ziq1Zi", "", )

RISCVBuiltin(vmv_v_x_i32m1, "q1ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m2, "q2Ziq2Zi", "", )

RISCVBuiltin(vmv_v_x_i32m2, "q2ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m4, "q4Ziq4Zi", "", )

RISCVBuiltin(vmv_v_x_i32m4, "q4ZiZi", "", )

RISCVBuiltin(vmv_v_v_i32m8, "q8Ziq8Zi", "", )

RISCVBuiltin(vmv_v_x_i32m8, "q8ZiZi", "", )

RISCVBuiltin(vmv_v_v_i64m1, "q1Wiq1Wi", "", )

RISCVBuiltin(vmv_v_x_i64m1, "q1WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m2, "q2Wiq2Wi", "", )

RISCVBuiltin(vmv_v_x_i64m2, "q2WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m4, "q4Wiq4Wi", "", )

RISCVBuiltin(vmv_v_x_i64m4, "q4WiWi", "", )

RISCVBuiltin(vmv_v_v_i64m8, "q8Wiq8Wi", "", )

RISCVBuiltin(vmv_v_x_i64m8, "q8WiWi", "", )

RISCVBuiltin(vmv_v_v_u8m1, "q1Ucq1Uc", "", )

RISCVBuiltin(vmv_v_x_u8m1, "q1UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m2, "q2Ucq2Uc", "", )

RISCVBuiltin(vmv_v_x_u8m2, "q2UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m4, "q4Ucq4Uc", "", )

RISCVBuiltin(vmv_v_x_u8m4, "q4UcUc", "", )

RISCVBuiltin(vmv_v_v_u8m8, "q8Ucq8Uc", "", )

RISCVBuiltin(vmv_v_x_u8m8, "q8UcUc", "", )

RISCVBuiltin(vmv_v_v_u16m1, "q1Usq1Us", "", )

RISCVBuiltin(vmv_v_x_u16m1, "q1UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m2, "q2Usq2Us", "", )

RISCVBuiltin(vmv_v_x_u16m2, "q2UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m4, "q4Usq4Us", "", )

RISCVBuiltin(vmv_v_x_u16m4, "q4UsUs", "", )

RISCVBuiltin(vmv_v_v_u16m8, "q8Usq8Us", "", )

RISCVBuiltin(vmv_v_x_u16m8, "q8UsUs", "", )

RISCVBuiltin(vmv_v_v_u32m1, "q1UZiq1UZi", "", )

RISCVBuiltin(vmv_v_x_u32m1, "q1UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m2, "q2UZiq2UZi", "", )

RISCVBuiltin(vmv_v_x_u32m2, "q2UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m4, "q4UZiq4UZi", "", )

RISCVBuiltin(vmv_v_x_u32m4, "q4UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u32m8, "q8UZiq8UZi", "", )

RISCVBuiltin(vmv_v_x_u32m8, "q8UZiUZi", "", )

RISCVBuiltin(vmv_v_v_u64m1, "q1UWiq1UWi", "", )

RISCVBuiltin(vmv_v_x_u64m1, "q1UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m2, "q2UWiq2UWi", "", )

RISCVBuiltin(vmv_v_x_u64m2, "q2UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m4, "q4UWiq4UWi", "", )

RISCVBuiltin(vmv_v_x_u64m4, "q4UWiUWi", "", )

RISCVBuiltin(vmv_v_v_u64m8, "q8UWiq8UWi", "", )

RISCVBuiltin(vmv_v_x_u64m8, "q8UWiUWi", "", )

RISCVBuiltin(vmv_v_v_f32m1, "q1fq1f", "", )

RISCVBuiltin(vfmv_v_f_f32m1, "q1ff", "", )

RISCVBuiltin(vmv_v_v_f32m2, "q2fq2f", "", )

RISCVBuiltin(vfmv_v_f_f32m2, "q2ff", "", )

RISCVBuiltin(vmv_v_v_f32m4, "q4fq4f", "", )

RISCVBuiltin(vfmv_v_f_f32m4, "q4ff", "", )

RISCVBuiltin(vmv_v_v_f32m8, "q8fq8f", "", )

RISCVBuiltin(vfmv_v_f_f32m8, "q8ff", "", )

RISCVBuiltin(vmv_v_v_f64m1, "q1dq1d", "", )

RISCVBuiltin(vfmv_v_f_f64m1, "q1dd", "", )

RISCVBuiltin(vmv_v_v_f64m2, "q2dq2d", "", )

RISCVBuiltin(vfmv_v_f_f64m2, "q2dd", "", )

RISCVBuiltin(vmv_v_v_f64m4, "q4dq4d", "", )

RISCVBuiltin(vfmv_v_f_f64m4, "q4dd", "", )

RISCVBuiltin(vmv_v_v_f64m8, "q8dq8d", "", )

RISCVBuiltin(vfmv_v_f_f64m8, "q8dd", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m1, "q1Scq1Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m1, "q1Ucq1Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m2, "q2Scq2Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m2, "q2Ucq2Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m4, "q4Scq4Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m4, "q4Ucq4Sc", "", )

RISCVBuiltin(vreinterpret_i8_u8_i8m8, "q8Scq8Uc", "", )

RISCVBuiltin(vreinterpret_u8_i8_u8m8, "q8Ucq8Sc", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m1, "q1Ssq1Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m1, "q1Usq1Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m2, "q2Ssq2Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m2, "q2Usq2Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m4, "q4Ssq4Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m4, "q4Usq4Ss", "", )

RISCVBuiltin(vreinterpret_i16_u16_i16m8, "q8Ssq8Us", "", )

RISCVBuiltin(vreinterpret_u16_i16_u16m8, "q8Usq8Ss", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m1, "q1UZiq1f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m1, "q1fq1UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m1, "q1Ziq1f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m1, "q1fq1Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m1, "q1Ziq1UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m1, "q1UZiq1Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m2, "q2UZiq2f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m2, "q2fq2UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m2, "q2Ziq2f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m2, "q2fq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m2, "q2Ziq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m2, "q2UZiq2Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m4, "q4UZiq4f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m4, "q4fq4UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m4, "q4Ziq4f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m4, "q4fq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m4, "q4Ziq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m4, "q4UZiq4Zi", "", )

RISCVBuiltin(vreinterpret_u32_f32_u32m8, "q8UZiq8f", "", )

RISCVBuiltin(vreinterpret_f32_u32_f32m8, "q8fq8UZi", "", )

RISCVBuiltin(vreinterpret_i32_f32_i32m8, "q8Ziq8f", "", )

RISCVBuiltin(vreinterpret_f32_i32_f32m8, "q8fq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_u32_i32m8, "q8Ziq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_i32_u32m8, "q8UZiq8Zi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m1, "q1UWiq1d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m1, "q1dq1UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m1, "q1Wiq1d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m1, "q1dq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m1, "q1Wiq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m1, "q1UWiq1Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m2, "q2UWiq2d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m2, "q2dq2UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m2, "q2Wiq2d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m2, "q2dq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m2, "q2Wiq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m2, "q2UWiq2Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m4, "q4UWiq4d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m4, "q4dq4UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m4, "q4Wiq4d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m4, "q4dq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m4, "q4Wiq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m4, "q4UWiq4Wi", "", )

RISCVBuiltin(vreinterpret_u64_f64_u64m8, "q8UWiq8d", "", )

RISCVBuiltin(vreinterpret_f64_u64_f64m8, "q8dq8UWi", "", )

RISCVBuiltin(vreinterpret_i64_f64_i64m8, "q8Wiq8d", "", )

RISCVBuiltin(vreinterpret_f64_i64_f64m8, "q8dq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_u64_i64m8, "q8Wiq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_i64_u64m8, "q8UWiq8Wi", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m1, "q1Ziq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m1, "q1Wiq1Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m1, "q1Ssq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m1, "q1Wiq1Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m1, "q1Scq1Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m1, "q1Wiq1Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m1, "q1Ssq1Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m1, "q1Ziq1Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m1, "q1Scq1Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m1, "q1Ziq1Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m1, "q1Scq1Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m1, "q1Ssq1Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m2, "q2Ziq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m2, "q2Wiq2Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m2, "q2Ssq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m2, "q2Wiq2Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m2, "q2Scq2Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m2, "q2Wiq2Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m2, "q2Ssq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m2, "q2Ziq2Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m2, "q2Scq2Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m2, "q2Ziq2Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m2, "q2Scq2Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m2, "q2Ssq2Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m4, "q4Ziq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m4, "q4Wiq4Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m4, "q4Ssq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m4, "q4Wiq4Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m4, "q4Scq4Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m4, "q4Wiq4Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m4, "q4Ssq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m4, "q4Ziq4Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m4, "q4Scq4Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m4, "q4Ziq4Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m4, "q4Scq4Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m4, "q4Ssq4Sc", "", )

RISCVBuiltin(vreinterpret_i32_i64_i32m8, "q8Ziq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i32_i64m8, "q8Wiq8Zi", "", )

RISCVBuiltin(vreinterpret_i16_i64_i16m8, "q8Ssq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i16_i64m8, "q8Wiq8Ss", "", )

RISCVBuiltin(vreinterpret_i8_i64_i8m8, "q8Scq8Wi", "", )

RISCVBuiltin(vreinterpret_i64_i8_i64m8, "q8Wiq8Sc", "", )

RISCVBuiltin(vreinterpret_i16_i32_i16m8, "q8Ssq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_i16_i32m8, "q8Ziq8Ss", "", )

RISCVBuiltin(vreinterpret_i8_i32_i8m8, "q8Scq8Zi", "", )

RISCVBuiltin(vreinterpret_i32_i8_i32m8, "q8Ziq8Sc", "", )

RISCVBuiltin(vreinterpret_i8_i16_i8m8, "q8Scq8Ss", "", )

RISCVBuiltin(vreinterpret_i16_i8_i16m8, "q8Ssq8Sc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m1, "q1UZiq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m1, "q1UWiq1UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m1, "q1Usq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m1, "q1UWiq1Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m1, "q1Ucq1UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m1, "q1UWiq1Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m1, "q1Usq1UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m1, "q1UZiq1Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m1, "q1Ucq1UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m1, "q1UZiq1Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m1, "q1Ucq1Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m1, "q1Usq1Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m2, "q2UZiq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m2, "q2UWiq2UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m2, "q2Usq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m2, "q2UWiq2Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m2, "q2Ucq2UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m2, "q2UWiq2Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m2, "q2Usq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m2, "q2UZiq2Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m2, "q2Ucq2UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m2, "q2UZiq2Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m2, "q2Ucq2Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m2, "q2Usq2Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m4, "q4UZiq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m4, "q4UWiq4UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m4, "q4Usq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m4, "q4UWiq4Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m4, "q4Ucq4UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m4, "q4UWiq4Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m4, "q4Usq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m4, "q4UZiq4Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m4, "q4Ucq4UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m4, "q4UZiq4Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m4, "q4Ucq4Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m4, "q4Usq4Uc", "", )

RISCVBuiltin(vreinterpret_u32_u64_u32m8, "q8UZiq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u32_u64m8, "q8UWiq8UZi", "", )

RISCVBuiltin(vreinterpret_u16_u64_u16m8, "q8Usq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u16_u64m8, "q8UWiq8Us", "", )

RISCVBuiltin(vreinterpret_u8_u64_u8m8, "q8Ucq8UWi", "", )

RISCVBuiltin(vreinterpret_u64_u8_u64m8, "q8UWiq8Uc", "", )

RISCVBuiltin(vreinterpret_u16_u32_u16m8, "q8Usq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_u16_u32m8, "q8UZiq8Us", "", )

RISCVBuiltin(vreinterpret_u8_u32_u8m8, "q8Ucq8UZi", "", )

RISCVBuiltin(vreinterpret_u32_u8_u32m8, "q8UZiq8Uc", "", )

RISCVBuiltin(vreinterpret_u8_u16_u8m8, "q8Ucq8Us", "", )

RISCVBuiltin(vreinterpret_u16_u8_u16m8, "q8Usq8Uc", "", )

RISCVBuiltin(vslideup_vx_i8m1, "q1Scq1Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m2, "q2Scq2Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m4, "q4Scq4Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i8m8, "q8Scq8Scz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m1, "q1Ssq1Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m2, "q2Ssq2Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m4, "q4Ssq4Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i16m8, "q8Ssq8Ssz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m1, "q1Ziq1Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m2, "q2Ziq2Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m4, "q4Ziq4Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i32m8, "q8Ziq8Ziz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m1, "q1Wiq1Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m2, "q2Wiq2Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m4, "q4Wiq4Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_i64m8, "q8Wiq8Wiz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m1, "q1Ucq1Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m2, "q2Ucq2Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m4, "q4Ucq4Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u8m8, "q8Ucq8Ucz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m1, "q1Usq1Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m2, "q2Usq2Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m4, "q4Usq4Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u16m8, "q8Usq8Usz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m1, "q1UZiq1UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m2, "q2UZiq2UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m4, "q4UZiq4UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u32m8, "q8UZiq8UZiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m1, "q1UWiq1UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m2, "q2UWiq2UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m4, "q4UWiq4UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_u64m8, "q8UWiq8UWiz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m1, "q1fq1fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m2, "q2fq2fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m4, "q4fq4fz", "", 2, )

RISCVBuiltin(vslideup_vx_f32m8, "q8fq8fz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m1, "q1dq1dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m2, "q2dq2dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m4, "q4dq4dz", "", 2, )

RISCVBuiltin(vslideup_vx_f64m8, "q8dq8dz", "", 2, )

RISCVBuiltin(vmset_m_b1, "q1b", "", )

RISCVBuiltin(vmset_m_b2, "q2b", "", )

RISCVBuiltin(vmset_m_b4, "q4b", "", )

RISCVBuiltin(vmset_m_b8, "q8b", "", )

RISCVBuiltin(vmset_m_b16, "q16b", "", )

RISCVBuiltin(vmset_m_b32, "q32b", "", )

RISCVBuiltin(vmset_m_b64, "q64b", "", )


#undef RISCVBuiltin